// #include "Arduino.h"
#include "funcoes_auxiliares.h"
#include "HardwareSerial.h"

extern HardwareSerial Serial;

void serialFlush(){
  while(Serial.available() > 0) {
    char t = Serial.read();
    Serial.print(" 0x");
    Serial.print(t, HEX);
  }
  Serial.println(".");
}  

void notificaErro(unsigned char * pacote_modbus){
    Serial.print("Dados rejeitados 0x");
    Serial.print(pacote_modbus[0], HEX);
    Serial.print(" 0x");
    Serial.print(pacote_modbus[1], HEX);
    serialFlush();
}