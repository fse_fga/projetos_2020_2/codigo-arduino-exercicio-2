/*
  UnB - Universidade de Brasilia
  FGA
  Exercício 2 - UART-MODBUS - Sistemas Embarcados
  Prof. Renato Sampaio
  
  Data: 24/02/2021

  Codigo do Microcontrolador Arduino para estabelecer comunicação serial 
  utilizando o protocolo MODBUS-RTU com o Raspberry Pi.
  
*/

#include "definicoes_gerais.h"
#include "crc16.h"
#include "funcoes_auxiliares.h"
#include "uart.h"
#include "comunicacao.h"

#include <avr/wdt.h>

/* --------------------------------------------------
 *  Configuração das Portas Seriais
 *  
 *  Serial = Comunicação Arduino - PC
 *  Serial1 = Comuinicação Arduino - Raspberry Pi
 ---------------------------------------------------*/
#define DEBUG
 
void setup() {
  // Configura Pino do LED
  pinMode(13, OUTPUT);
  // Configura Serial - PC
#ifdef DEBUG  
  Serial.begin(9600);
  while (!Serial) {
    ; // Aguarda inicialização da Serial
  }
#endif  
  // Configura Serial - Raspberry Pi
  Serial1.begin(9600);

  // Configura o Watchdog Timer de 8 segundos
  wdt_enable(WDTO_8S);
}

/*------------------------------------------------------
 *  Configuração de Variáveis Globais
 ----------------------------------------------------- */
long int dado_inteiro = 41987;
float dado_real = 3.141516;
char dado_string[] = "Mensagem de Teste MODBUS via UART!";

char dado_string_retorno[] = "String Recebida: ";

long int dado_inteiro_recebido = 0;
float dado_float_recebido = 0.0;
char dado_string_recebido[] = "";
int tamanho_string;

// Pacote
unsigned char pacote_modbus[200];

char dados_recebidos[256];
char string_recebida[256];

/*------------------------------------------------------
 *  Loop Principal
 ----------------------------------------------------- */
void loop() {

  wdt_reset();
  
  if(Serial1.available()) {
    pacote_modbus[0] = le_byte_uart();
    pacote_modbus[1] = le_byte_uart();

    if(pacote_modbus[0] == -1 || pacote_modbus[1] == -1){
      notificaErro(pacote_modbus);
    }

    if((pacote_modbus[0] != CODIGO_SERVIDOR) || ((pacote_modbus[1] != CODIGO_CMD_SOLICITA) && (pacote_modbus[1] != CODIGO_CMD_ESCREVE)))
    {
      notificaErro(pacote_modbus);
    }
    else
    {
        pacote_modbus[2] = le_byte_uart();

        Serial.print("Comando Recebido: ");
        Serial.println(pacote_modbus[2], HEX);

        switch (pacote_modbus[2]) {
          case CMD_SOLICITA_INT:
            if(verifica_crc(pacote_modbus, 3)){
                Serial.println("Solicitou um INTEIRO");
                envia_int(pacote_modbus, dado_inteiro);
            }
            break;

          case CMD_SOLICITA_FLOAT:
            if(verifica_crc(pacote_modbus, 3)){
                Serial.println("Solicitou um FLOAT");
                envia_float(pacote_modbus, dado_real);
            }
            break;

          case CMD_SOLICITA_STRING:
            if(verifica_crc(pacote_modbus, 3)){
                Serial.print("Solicitou uma STRING: ");
                Serial.println(strlen(dado_string)+1);
                envia_string(pacote_modbus, dado_string, strlen(dado_string)+1);
            }
            break;

          case CMD_ENVIA_INT:
            dado_inteiro_recebido = 0;
            dado_inteiro_recebido = le_inteiro();
            memcpy(&pacote_modbus[3], &dado_inteiro_recebido, 4);
            if(verifica_crc(pacote_modbus, 7)){
              Serial.print("Enviou um INTEIRO = ");
              Serial.println(dado_inteiro_recebido);
              envia_int(pacote_modbus, dado_inteiro_recebido*2);
            }            
            break;
          case CMD_ENVIA_FLOAT:
            dado_float_recebido = 0.0;
            dado_float_recebido = le_float();
            memcpy(&pacote_modbus[3], &dado_float_recebido, 4);
            if(verifica_crc(pacote_modbus, 7)){
              Serial.print("Enviou um FLOAT = ");
              Serial.println(dado_float_recebido);
              envia_float(pacote_modbus, dado_float_recebido*2);
            }
            break;

          case CMD_ENVIA_STRING:
            tamanho_string = le_string(string_recebida);
            memcpy(&pacote_modbus[3], &tamanho_string, 1);
            memcpy(&pacote_modbus[4], &string_recebida, tamanho_string);
            if(verifica_crc(pacote_modbus, 4+tamanho_string)){
              envia_string(pacote_modbus, string_recebida, strlen(string_recebida)+1);
            }
            break;

          default:
            Serial.print("Pacote invalido!!!");
            notificaErro(pacote_modbus);
            break;
          
        } // End Switch

    } // End if else (Vertifica cabeçalho)
  } // End IF INICIAL
  delay(10);
}
