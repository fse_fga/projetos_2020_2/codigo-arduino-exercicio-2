/*
 * comunicacao.h
 *
 *  Created on: 24/02/2021
 *      Author: Renato Coral Sampaio
 */

#ifndef COMUNICACAO_H_
#define COMUNICACAO_H_

long le_inteiro();
float le_float();
int le_string(char * string_recebida);

void envia_mensagem(char * comando, unsigned char *dado, int tamanho);
void envia_int(char * comando, long int dado);
void envia_float(char * comando, float dado);
void envia_string(char * comando, char * mensagem, int tamanho);

#endif /* COMUNICACAO_H_ */